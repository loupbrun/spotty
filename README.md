Spotty
===

## Getting started

The live code may be edited directly in the `app/` folder. To run a development server, see the following instructions below.

### Requirements

You must have Node.js installed in order to run the provided server and to run the development tools.

If you are ready to proceed, install the following command-line tools:

```bash
npm install -g grunt-cli bower # the -g flag makes the modules available from the command line
```

### Installation

Clone this repository, then navigate to the `tools/` directory and run

```bash
npm install
bower install # optional, downloads the third-party libraries for development
```

Once this is complete, run `grunt serve` to start a local development server that live reloads on `localhost:9000`.
