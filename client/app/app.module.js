define(['angular'], function (ng) {

  var appName = 'spotty.app',
    requiredModules = [
        'app/slides/slides.module',
      'app/player/player.module'
      ];

  // Load all the modules we need to start the Angular application
  require(requiredModules, function (slidesModule, playerModule) {
    ng.module(appName, [
      slidesModule,
      playerModule
    ]);

    ng.bootstrap(document.documentElement, [appName]);
  });

});
