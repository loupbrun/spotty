define([], function () {
  /**
   * Player Controller, handles the player API.
   * @controller
   * @constructor
   */
  function PlayerController() {
    var player = this;

    /**
     * @property videoElement
     * @type {HTMLElement}
     * @default {Object}
     */
    player.videoElement = {};
    /**
     * @property currentTime
     * @type {Number}
     * @default 0
     */
    player.currentTime = 0;

    /**
     * @method play
     * @see {Function} play
     * @return {void}
     */
    player.play = play;

    /**
     * @method pause
     * @see {Function} pause
     * @return {void}
     */
    player.pause = pause;

    /**
     * @method isPaused
     * @see {Function} isPaused
     * @return {Boolean}
     */
    player.isPaused = isPaused;

    /**
     * @method togglePlay
     * @see {Function} togglePlay
     * @return {void}
     */
    player.togglePlay = togglePlay;

    /**
     * @method updateTime
     * @see {Function} updateTime
     * @return {Number}
     */
    player.updateTime = updateTime;

    /**
     * @method getCurrentTIime
     * @see {Function} getCurrent
     * @return {Boolean}
     */
    player.getCurrentTime = getCurrentTime;

    /**
     * @method setElement
     * @see {Function} setElement
     */
    player.setElement = setElement;

    // Implementation of the HTML5 video API
    /**
     * @name play
     * @type {Function}
     * @return {void}
     */
    function play() {
      player.videoElement.play();
    }

    /**
     * @name pause
     * @type {Function}
     * @return {void}
     */
    function pause() {
      player.videoElement.pause();
    }

    /**
     * @name togglePlay
     * @type {Function}
     * @return {void}
     */
    function togglePlay() {
      return player.isPaused() ? play() : pause();
    }

    /**
     * @name isPaused
     * @type {Function}
     * @return {Boolean}
     */
    function isPaused() {
      return player.videoElement.paused;
    }

    /**
     * @name getCurrentTime
     * @type {Function}
     * @return {Number}
     */
    function getCurrentTime() {
      return player.videoElement.currentTime;
    }

    /**
     * @name setElment
     * @type function
     * @param {HTMLElement} newElement
     */
    function setElement(newElement) {
      player.videoElement = newElement;
    }

    /**
     * @name updateTime
     * @type {Function}
     * @param {Number} newTime
     * @return {Number}
     */
    function updateTime(newTime) {
      player.currentTime = newTime;
      return player.currentTime;
    }
  }

  return PlayerController;
});
