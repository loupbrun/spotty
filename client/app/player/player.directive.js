define([
  'angular',
  './player.controller'
], function (ng, PlayerController) {
  /**
   * @ngdoc directive
   * @type {Function}
   * @return {Object}
   */
  function playerDirective() {
    return {
      restrict: 'EA',
      templateUrl: '/app/player/awesome-player.html',
      controller: PlayerController,
      controllerAs: 'player',
      link: playerLink
    };
  }

  /**
   * @ngdoc link
   * @type {Function}
   */
  function playerLink(scope, element, attrs, player) {
    player.setElement(element.find('video')[0]);

    ng.element(player.videoElement).on('timeupdate', function (event) {
      // Bind the native element's `timeupdate` event to update the player's
      // `currentTime` property
      player.updateTime(event.target.currentTime);
      // Propagate the change, since we're listening to an event from the DOM 
      scope.$apply();
    });
  }

  return playerDirective;
});
