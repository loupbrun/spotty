define([
  'angular',
  './player.directive',
], function (ng, playerDirective) {
  var moduleName = 'playerModule';

  ng.module(moduleName, [])
    .directive('awesomePlayer', playerDirective);

  return moduleName;
});
