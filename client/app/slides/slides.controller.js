define(['angular'], function (ng) {
  /**
   * Slides Controller
   * @constructor
   * @inject SlidesService
   */
  function SlideController(SlidesService) {

    var slidesVM = this;

    /**
     * @property slides
     * @type {Array}
     */
    slidesVM.slides = [];

    /**
     * @method fetchSlides
     * @see fetchSlides
     */
    slidesVM.getSlides = getSlides;

    /**
     * @method addSlide
     * @param {Object} slide
     * @return {void}
     */
    slidesVM.addSlide = function (slide) {
      slidesVM.slides.push(slide);
    };

    /**
     * @method toggleSlide
     * @see {Function} toggleSlide
     */
    slidesVM.toggleSlide = toggleSlide;

    /**
     * @method isInTimeView
     * @see {Function} isInTimeView
     */
    slidesVM.isInTimeView = isInTimeView;

    collapseAllSlides();
    populateSlides();

    /**
     * Fetch the slides and add them to the slides collection.
     * @type {Function}
     */
    function populateSlides() {
      return getSlides().then(function () {
        console.log('Slides fetched and attached onto the controller.');
      });
    }

    /**
     * Fetch the slides with the service and return a promise object.
     * @type {Function}
     * @return {Object}
     */
    function getSlides() {
      return SlidesService.fetchSlides('/data/slides.json').success(function (data) {
        slidesVM.slides = data;

        return slidesVM.slides;
      });
    }

    // Toggle the current slide's state
    /**
     * Collapse all slides except the selected one
     * @function toggleSlide
     * @param {Number} targetSlide
     */
    function toggleSlide(targetSlide) {
      console.log('toggle slide with value ', targetSlide);
      collapseAllSlides(targetSlide);

      slidesVM.slides[targetSlide].isInView = !slidesVM.slides[targetSlide].isInView;
    }

    /**
     * Collapse all the slides.
     * @type {Function}
     * @param {Number} exceptMe
     */
    function collapseAllSlides(exceptMe) {
      ng.forEach(slidesVM.slides, function (slide, index) {
        if (exceptMe && index !== exceptMe) {
          slidesVM.slides[index].isInView = false;
        }
      });
    }

    /**
     * Check if a slide is in the current time frame.
     * @type {Function}
     * @param {Object} slide
     * @param {Number} currentTime
     */
    function isInTimeView(slide, currentTime) {
      return slide.time.start < currentTime && currentTime < slide.time.end;
    }
  }

  /**
   * Call the $injector to inject a service
   * @property {Array} $inject
   */
  SlideController.$inject = ['SlidesService'];

  return SlideController;
});
