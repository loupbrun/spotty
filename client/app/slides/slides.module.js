define([
  'angular',
  './slides.service',
  './slides.controller'
], function (ng, SlidesService, SlidesController) {
  var moduleName = 'slides.module';

  ng.module(moduleName, [])
    .service('SlidesService', SlidesService)
    .controller('SlidesController', SlidesController);

  return moduleName;
});
