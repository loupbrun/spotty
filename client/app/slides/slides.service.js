define([], function () {
  /**
   * @ngdoc service
   * @inject $http
   */
  function SlidesService($http) {
    this.fetchSlides = fetchSlides;

    /**
     * Fetch slides and return a promise object with the data.
     * @type {Function}
     * @param {String} url
     * @return {Object}
     */
    function fetchSlides(url) {
      return $http.get(url);
    }
  }

  SlidesService.$inject = ['$http'];

  return SlidesService;
});
