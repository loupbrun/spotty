require.config({
  baseUrl: 'lib',
  paths: {
    app: '../app'
  },
  shim: {
    angular: {
      exports: 'angular'
    }
  }
});

// Let's start the app
require(['app/app.module']);
