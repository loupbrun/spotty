'use strict';

module.exports = function (grunt) {

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  var userConfig = {
      client: '../client'
    },
    bowerrc = grunt.file.readJSON('.bowerrc'),
    pkg = grunt.file.readJSON('package.json');


  grunt.initConfig({

    config: userConfig,
    pkg: pkg,
    bower: bowerrc,

    copy: {
      lib: {
        files: [{
          expand: true,
          cwd: '<%= bower.directory %>',
          src: [
            'angular/angular.js',
            'bootstrap/dist/css/bootstrap.min.css',
            'requirejs/require.js'
          ],
          flatten: true,
          dest: '<%= config.client %>/lib'
        }, {
          expand: true,
          cwd: '<%= bower.directory %>',
          src: [
            'bootstrap/dist/fonts/*',
          ],
          flatten: true,
          dest: '<%= config.client %>/fonts'
        }]
      }
    },

    connect: {
      options: {
        port: 9000,
        open: true,
        livereload: 35729,
        // Change this to '0.0.0.0' to access the server from outside
        hostname: 'localhost'
      },
      livereload: {
        options: {
          middleware: function (connect) {
            return [
              connect().use('/bower_components', connect.static('./../bower_components')),
              connect.static('./' + userConfig.client + '/')
            ];
          }
        }
      }
    },

    watch: {
      js: {
        files: ['<%= config.client %>/app{,*/,*/*/}*.js'],
        options: {
          livereload: true
        }
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '<%= config.client %>/{,*/,*/*/}*.html'
        ]
      }
    },
  });

  grunt.registerTask('serve', 'start the server and preview the client', function () {
    grunt.task.run([
      'copy:lib',
      'connect:livereload',
      'watch'
    ]);
  });
};
